from timeit import timeit

print('Running 2 - IO Gevent')
io_gevent = timeit('run_io_gevent()', setup='from geventex.ex2_io import run_io_gevent', number=1)
print(io_gevent)

print()

print('Running 2 - IO Threads')
io_threads = timeit('run_io_threads()', setup='from geventex.ex2_io_threads import run_io_threads', number=1)
print(io_threads)
