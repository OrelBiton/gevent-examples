from threading import Thread
import socket


def get_ip_from_url(idx, url):
    hostname = socket.gethostbyname(url)
    print('{host} ({idx} {url})'.format(host=hostname, idx=idx, url=url))


def run_io_threads():
    urls = ['google.com', 'walla.co.il', 'reddit.com', 'yoga.com']
    threads = [Thread(target=get_ip_from_url, args=(idx, url)) for idx, url in enumerate(urls)]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()


if __name__ == '__main__':
    run_io_threads()
