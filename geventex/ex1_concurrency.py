import gevent

"""
This example serves to show the bare-bones of Greenlets and explicit context-switching. 
"""


def a():
    print("I am about to make an explicit context switch to b!")
    gevent.sleep(3)
    print("I'm running in A!")


def b():
    print("Hello, my name is B!")
    gevent.sleep(3)
    print("Back in B!")


gevent.joinall([
    gevent.spawn(a),
    gevent.spawn(b)
])
