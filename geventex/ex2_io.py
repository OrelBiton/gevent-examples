import gevent
import gevent.socket as socket

"""
This networking example really shows how powerful Gevent is when using with sockets.
Note we are using 'gevent.socket' (which uses the same interface as the standard library 'socket')!
"""


def get_ip_from_url(idx, url):
    hostname = socket.gethostbyname(url)
    print('{host} ({idx} {url})'.format(host=hostname, idx=idx, url=url))


def run_io_gevent():
    urls = ['google.com', 'walla.co.il', 'reddit.com', 'yoga.com']
    jobs = [gevent.spawn(get_ip_from_url, idx, url) for idx, url in enumerate(urls)]

    print('Using {}'.format(socket.__name__))
    gevent.joinall(jobs)


if __name__ == '__main__':
    run_io_gevent()
