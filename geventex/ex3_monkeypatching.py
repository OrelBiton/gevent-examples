from gevent import monkey

import socket
import subprocess

'''
Gevent provides a solution to "patch" the standard modules 
for Greenlets to work effortlessly with existing code.
'''

print(socket.socket)

print('After monkey patch')
monkey.patch_socket()
print(socket.socket)
print()

print(subprocess.Popen)
print('After monkey patch')
monkey.patch_subprocess()
print(subprocess.Popen)
